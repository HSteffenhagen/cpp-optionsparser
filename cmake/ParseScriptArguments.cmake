macro(parse_script_arguments prefix options one_value_keywords multi_value_keywords)
  set(ARGUMENTS "")

  # starting at 3 because we want to ignore cmake -P <script-name>
  foreach(N RANGE 3 ${CMAKE_ARGC})
    set(ARG_N "${CMAKE_ARGV${N}}")
    list(APPEND ARGUMENTS "${ARG_N}")
  endforeach()

  cmake_parse_arguments(
    ${prefix}
    "${options}"
    "${one_value_keywords}"
    "${multi_value_keywords}"
    "${ARGUMENTS}")
endmacro()
