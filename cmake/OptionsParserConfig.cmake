if(NOT TARGET OptionsParser::OptionsParser)
  find_package(fmt REQUIRED)
  include("${CMAKE_CURRENT_LIST_DIR}/OptionsParserTargets.cmake")
endif()