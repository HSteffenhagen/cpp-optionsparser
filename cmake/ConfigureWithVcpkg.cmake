cmake_minimum_required(VERSION 3.10)

macro(print_debug)
  message(${ARGV0} = "${${ARGV0}}")
endmacro()

set(ArgumentPrefix VcpkgConfigure)

macro(required_argument)
  if(NOT "${ArgumentPrefix}_${ARGV0}")
    message(FATAL_ERROR "missing required argument ${ARGV0}")
  endif()
endmacro()

macro(default_argument)
  if(NOT "${ArgumentPrefix}_${ARGV0}")
    set(${ArgumentPrefix}_${ARGV0} ${ARGV1})
  endif()
endmacro()

include("${CMAKE_CURRENT_LIST_DIR}/ParseScriptArguments.cmake")


parse_script_arguments(${ArgumentPrefix} "" "VCPKG_URL;VCPKG_TAG" "DEPENDENCIES")

default_argument(VCPKG_URL "https://github.com/Microsoft/vcpkg.git")
set(VcpkgConfigure_CMAKE_ARGS ${VcpkgConfigure_UNPARSED_ARGUMENTS})

if(WIN32)
  set(VCPKG_BINARY_NAME "vcpkg.exe")
else()
  set(VCPKG_BINARY_NAME "vcpkg")
endif()

message("Checking for vcpkg")
if(NOT EXISTS "vcpkg/${VCPKG_BINARY_NAME}")
  message("vcpkg not found, downloading and building it")

  find_program(GIT_BIN NAMES git)
  if(NOT GIT_BIN)
    message(FATAL_ERROR "git not found, this is required")
  endif()

  message("cloning...")
  execute_process(
    COMMAND "${GIT_BIN}" clone "${VcpkgConfigure_VCPKG_URL}" vcpkg
    RESULT_VARIABLE CLONE_RESULT)
  if(NOT ${CLONE_RESULT} EQUAL 0)
    message(FATAL_ERROR "git clone failed")
  endif()

  if(VcpkgConfigure_VCPKG_TAG)
    message("checking out right tag")
    execute_process(
      COMMAND "${GIT_BIN}" checkout "${VcpkgConfigure_VCPKG_TAG}"
      WORKING_DIRECTORY vcpkg
      RESULT_VARIABLE CHECKOUT_RESULT)
    if(NOT ${CHECKOUT_RESULT} EQUAL 0)
      message(FATAL_ERROR "git checkout failed")
    endif()
  endif()

  if(WIN32)
    message("[Windows specific] bootstrapping vcpkg")
    execute_process(
      COMMAND "cmd /c bootstrap-vcpkg.bat"
      WORKING_DIRECTORY vcpkg)
  else()
    message("*nix-like bootstrapping vcpkg")
    execute_process(
      COMMAND "./bootstrap-vcpkg.sh"
      WORKING_DIRECTORY vcpkg)
  endif()
endif()


foreach(DEPENDENCY IN LISTS VcpkgConfigure_DEPENDENCIES)
  execute_process(
    COMMAND "./${VCPKG_BINARY_NAME}" install "${DEPENDENCY}"
    WORKING_DIRECTORY vcpkg)
endforeach()

# delete unneeded built artifacts
file(REMOVE_RECURSE "vcpkg/buildtrees")

execute_process(
  COMMAND ${CMAKE_COMMAND}
          "-DCMAKE_TOOLCHAIN_FILE=${CMAKE_CURRENT_BINARY_DIR}/vcpkg/scripts/buildsystems/vcpkg.cmake"
          ${VcpkgConfigure_CMAKE_ARGS})
