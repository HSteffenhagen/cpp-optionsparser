#include <catch.hpp>
#include <library.h>

TEST_CASE("greeting contains hello")
{
    using Catch::Matchers::Contains;
    REQUIRE_THAT(greet("hannes"), Contains("Hello"));
}

TEST_CASE("greeting contains the name of the person that was greeted")
{
    using Catch::Matchers::Contains;
    REQUIRE_THAT(greet("paul"), Contains("paul"));
}
