#ifndef OPTIONS_PARSER_LIBRARY_H
#define OPTIONS_PARSER_LIBRARY_H

#include <string>
#include <string_view>

std::string greet(std::string_view name);

#endif //OPTIONS_PARSER_LIBRARY_H
