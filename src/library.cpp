#include <library.h>
#include <fmt/format.h>

std::string greet(std::string_view name) {
    return fmt::format("Hello, {}!", name);
}
